package TestSuite;


import Pages.AccountPage;
import Pages.MainPage;
import Services.CustomServices;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignInTest extends BaseTest {

    @Test

    void test (){

        CustomServices customServices = new CustomServices(driver);
        customServices.goToURL("https://rozetka.com.ua/ua/");


        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);

        mainPage.clickSignInLink();
        mainPage.enterLoginField();
        mainPage.enterPassword();
        mainPage.clickRememberMeButton();
        mainPage.clickSubmitButton();
        mainPage.clickUserLoggedIn();

        AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);

        accountPage.waitForUserName();
        accountPage.getName();
        accountPage.clickSignOutButton();




    }

}
