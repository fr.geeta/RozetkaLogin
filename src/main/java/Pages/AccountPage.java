package Pages;


import Services.WaiterServices;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import sun.jvm.hotspot.utilities.Assert;

public class AccountPage {

    private final WebDriver driver;

    public AccountPage(WebDriver driver) {
        this.driver = driver;
    }

    WaiterServices waiterServices = new WaiterServices();

    @FindBy(xpath = "//*[@id=\"personal_information\"]/div/div[1]/div[2]/div/div/div[1]/div[2]/div\n")
    private WebElement userName;

    public void waitForUserName(){
        waiterServices.waitForText(userName, driver);
    }
    public void getName(){
        String expectedName = userName.getText();
        Assert.that(true, "Test User");

    }
    @FindBy (id = "profile_signout")
    private WebElement signOutButton;

    public void clickSignOutButton(){
        signOutButton.click();
    }


}
