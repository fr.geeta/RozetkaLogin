package Pages;

import Services.WaiterServices;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import sun.jvm.hotspot.utilities.Assert;

public class MainPage {

    private final WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;}

    WaiterServices waitServices = new WaiterServices();

    @FindBy(id = "header_user_menu_parent")
    private WebElement signInLink;

    public void clickSignInLink (){
        waitServices.waitForElementClickable(signInLink, driver);
        signInLink.click();
    }

    @FindBy(xpath = "//*[@id=\"popup_signin\"]/div[1]/div[1]/input")
    private WebElement login;

    public void enterLoginField(){
        login.sendKeys("testusertemplate@gmail.com");
    }

    @FindBy(xpath = "//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[1]/input")
    private WebElement password;

    public void enterPassword (){
        password.sendKeys("Testpassword1");
    }

    @FindBy (xpath = "//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[1]/label/span")
    private WebElement rememberMeButton;

    public void clickRememberMeButton(){
        rememberMeButton.click();
    }

    @FindBy(xpath = "//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[2]/div/span/button")
    private WebElement submitButton;

    public void clickSubmitButton(){
        submitButton.click();
    }

    @FindBy(xpath = "//*[@id=\"header_user_menu_parent\"]/a")
    private WebElement userLoggedIn;

    public void clickUserLoggedIn(){
        waitServices.waitForElementClickable(userLoggedIn, driver);
        userLoggedIn.click();
    }

}
